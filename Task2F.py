import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels

def run():
    """Requirements for Task2F"""

    #Build list of stations
    stations = build_station_list()

    update_water_levels(stations)
    top_5_stations = stations_highest_rel_level(stations, 5)

    for station in top_5_stations:
        station = station[0]
        # Fetch data over past 2 days
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))

        plot_water_level_with_fit(station,dates,levels,4)

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    print()
    run()