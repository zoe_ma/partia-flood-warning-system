#Task1C
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius
from floodsystem.station import MonitoringStation

def runC():
    #build a list of stations within 10 km of the Cambridge city centre (coordinate (52.2053, 0.1218))
    centre = (52.2053, 0.1218)
    r = 10
    stations = build_station_list()
    # print the names of the stations, listed in alphabetical order
    print(sorted(stations_within_radius(stations, centre, r)))

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    print()
    runC()