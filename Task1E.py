from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def run():
    """Requirements for Task1E"""

    #Build list of stations
    stations = build_station_list()

    #Print list of rivers by station number where N=9
    N = 9
    print("First " + str(N) + " rivers sorted by descending number of stations: {}".format(rivers_by_station_number(stations,N)))
    print()

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    print()
    run()
