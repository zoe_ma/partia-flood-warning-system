from floodsystem.geo import stations_by_town
from floodsystem.stationdata import build_station_list
from floodsystem.flood import average_relative_water_level_by_town
from floodsystem.flood import water_change
from floodsystem.flood import water_change_change
from floodsystem.stationdata import update_water_levels
from floodsystem.classify import filter_at_risk
from floodsystem.classify import determine_risk_level


def run():
    """Task2G"""

    #Build list of stations
    stations = build_station_list()

    update_water_levels(stations)

    dict_town_stations = stations_by_town(stations)
    list_sorted_rel_water_level_by_town = average_relative_water_level_by_town(dict_town_stations)
    towns_at_risk = filter_at_risk(list_sorted_rel_water_level_by_town)
    dict_towns_water_change = water_change(towns_at_risk, dict_town_stations)
    dict_towns_water_change_change = water_change_change(towns_at_risk, dict_town_stations)
    towns_risk_level = determine_risk_level(towns_at_risk, dict_towns_water_change, dict_towns_water_change_change)

    for key, value in towns_risk_level.items():
        print(str(key) + ": " + value)
        print()



if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    print()
    run()
    