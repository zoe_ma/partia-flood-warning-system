#Task2C
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import update_water_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    stations_highest_rel_level(stations, 10)
    l = stations_highest_rel_level(stations, 10)
    for i in l:
        print(i[0].name, i[1])

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    print()
    run()