from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def run():
    """Requirements for Task1D"""

    #Build list of stations
    stations = build_station_list()

    #Create a sorted list of rivers with stations
    list_rivers = list(rivers_with_station(stations))
    list_rivers.sort()

    #Create a dictionary of format River:[Stations]
    dict_river_stations = stations_by_river(stations)

    #Print number of rivers with stations
    print("Number of rivers with stations: {}".format(len(list_rivers)))
    print()

    #Print the first 10 elements in the list of rivers
    print("First 10 rivers alphabetically: {}".format(list_rivers[0:10]))
    print()

    #Create list of rivers to print
    list_rivers_to_print = ["River Aire", "River Cam", "River Thames"]

    for index in list_rivers_to_print:

        #Create list for station names
        list_river_stations_names = []

        for station in dict_river_stations[index]:
            list_river_stations_names.append(station.name)

        #Sort the list of stations
        list_river_stations_names.sort()
        
        print(index + ": {}".format(list_river_stations_names))
        print()

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    print()
    run()
