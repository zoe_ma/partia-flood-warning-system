#Task2B
from floodsystem.stationdata import build_station_list
from floodsystem import datafetcher
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import update_water_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    #for station in stations:
    #    print(station.relative_water_level())
    #print(stations[24].latest_level)
    list_sorted = stations_level_over_threshold(stations, 0.8)
    for i in list_sorted:
        print(i[0].name, i[1])

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    print()
    run()