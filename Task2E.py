#Task2E
import datetime
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list

def run():
    #Build list of stations
    stations = build_station_list()
     
    stations_s = stations_highest_rel_level(stations, 5)
    stations_selected = []
    for i in stations_s:
        stations_selected.append(i[0])

    for station in stations_selected:
        dt = 10
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        plot_water_levels(station, dates, levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    print()
    run()