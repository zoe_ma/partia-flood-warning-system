from floodsystem.geo import stations_by_distance
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list


def run1B():
    #sort stations by distance
    dist = stations_by_distance(build_station_list(use_cache=True), [52.2053,0.1218])
    #the closest 10 stations
    x = []
    #the furthest 10 stations
    y = []
    #add the name , town and distance from the dist list
    for (a,b) in dist[:10]:
        x.append((a.name, a.town, b))
    for (a,b) in dist[-10:]:
        y.append((a.name, a.town, b))
    print("***10 closest stations***,", x)
    print("***10 furthest stations***,", y)

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    print()
    run1B()