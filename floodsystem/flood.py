#This is a submodule called flood
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.utils import sorted_by_key
from floodsystem.analysis import polyfit

def stations_level_over_threshold(stations, tol):
    #create a list of stations with consistent range
    stations_consistent_range = []
    list_tuples = []
    for station in stations:
        if station.typical_range_consitent():
            stations_consistent_range.append(station)
    #for stations with consistent range, create a list of tuples as 
    #whose relative water level is higher than tol(float)
    # (station(object),  relative water level of the station(float))
    for station in stations_consistent_range:
        try:
            if station.relative_water_level() > tol:
                list_tuples.append((station, station.relative_water_level()))
    
        except:
            pass
    #sort the list of tuples by the second index in descending order
    return sorted_by_key(list_tuples, 1, reverse = True)


def stations_highest_rel_level(stations, N):
    
    stations_highest_level = []
    stations_highest_level_ = []
    #create a list of stations with consistent range
    for station in stations:
        if station.typical_range_consitent():
            stations_highest_level.append(station)
    #for stations with consistent range, create a list of tuples as 
    # (station(object),  relative water level of the station(float))
    for station in stations_highest_level:
        try:
            if type(station.relative_water_level()) == float:
                stations_highest_level_.append((station, station.relative_water_level()))
        except:
            pass
    #print(stations_highest_level)
    #sort the list of tuples by the second index in descending order
    first_N_stations = sorted_by_key(stations_highest_level_,1,reverse = True)
    return first_N_stations[:N]
    
def average_relative_water_level_by_town(dict_town_stations):

    dict_town_water_level = {}

    for key, value in dict_town_stations.items():
        total = 0
        stations = 0
        for i in range(len(value)):
            if value[i].relative_water_level() != None:
                total += value[i].relative_water_level()
                stations += 1
        if stations != 0:
            average_relative_water_level = total/stations
            dict_town_water_level[key] = average_relative_water_level

    list_town_water_level = []

    #Convert dictionary to list
    for key, item in dict_town_water_level.items():
        list_town_water_level.append((key,item))

    #Define sort function
    def second_index(index):
        return index[1]

    #Sort list by station number descending
    list_town_water_level.sort(key=second_index, reverse=True)

    return list_town_water_level
    
def water_change(towns_at_risk, dict_town_stations):

    dict_towns_water_change = {}

    for index in towns_at_risk:
        total = 0
        stations = 0
        for station in dict_town_stations[index[0]]:
            stations += 1
            dt = 2
            dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
            poly, d0 = polyfit(dates, levels ,1)
            total += poly[1]
        dict_towns_water_change[index[0]] = total/stations
    return dict_towns_water_change

def water_change_change(towns_at_risk, dict_town_stations):

    dict_towns_water_change_change = {}

    for index in towns_at_risk:
        total = 0
        stations = 0
        for station in dict_town_stations[index[0]]:
            stations += 1
            dt = 2
            dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
            poly, d0 = polyfit(dates, levels ,2)
            total += 2*poly[2]
        dict_towns_water_change_change[index[0]] = total/stations
    return dict_towns_water_change_change