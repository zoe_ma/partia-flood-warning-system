def filter_at_risk(towns_flood_levels):

    if towns_flood_levels[0][1] >= 0.8:
        i = 0
        while  i < len(towns_flood_levels) and towns_flood_levels[i][1] >= 0.8:
            i += 1
        towns_flood_levels = towns_flood_levels[:i]
    
    return towns_flood_levels

def determine_risk_level(towns_at_risk, dict_towns_water_change, dict_towns_water_change_change):

    dict_towns_risk_level = {}

    #assign no risk to all towns in list
    for i in range(len(towns_at_risk)):
        dict_towns_risk_level[towns_at_risk[i][0]] = 0
    
    #evaluate risk level based on water level
    for i in range(len(towns_at_risk)):
        if towns_at_risk[i][1] >= 1:
            dict_towns_risk_level[towns_at_risk[i][0]] += 1
        if towns_at_risk[i][1] >= 1.2:
            dict_towns_risk_level[towns_at_risk[i][0]] += 1 
    
    #evalaute risk level based on water level change
    for key, value in dict_towns_water_change.items():
        if value > 0:
            dict_towns_risk_level[key] += 1
        elif value < 0:
            dict_towns_risk_level[key] -= 1

    #evalaute risk level based on water level change change
    for key, value in dict_towns_water_change_change.items():
        if value > 0:
            dict_towns_risk_level[key] += 1
        elif value < 0:
            dict_towns_risk_level[key] -= 1

    list_towns_risk_level = []

    #Convert dictionary to list
    for key, item in dict_towns_risk_level.items():
        list_towns_risk_level.append((key,item))

    #Define sort function
    def second_index(index):
        return index[1]

    #Sort list by station number descending
    list_towns_risk_level.sort(key=second_index, reverse=True)

    dict_towns_risk_warning = {}

    for key, value in list_towns_risk_level:
        if value == 1:
            dict_towns_risk_warning[key] = "low"
        elif value == 2:
            dict_towns_risk_warning[key] = "moderate"
        elif value == 3:
            dict_towns_risk_warning[key] = "high"
        elif value == 4:
            dict_towns_risk_warning[key] = "severe"
        #Enable below statement to set minium risk level to low
        #else:
        #    dict_towns_risk_warning[key] = "low"

    #Enable below statements to allow manual analysis
    #print(towns_at_risk)
    #print(dict_towns_water_change)
    #print(dict_towns_water_change_change)

    return dict_towns_risk_warning