# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine 

#Task1B
def stations_by_distance(stations, p):

    station_name = []
    station_distance = []

    for i in range(len(stations)):
        stat = (stations[i].coord[0], stations[i].coord[1])
        p = (p[0], p[1])
        distance = haversine(stat, p)
        station_distance.append(distance)
        station_name.append(stations[i])

    return sorted_by_key(list(zip(station_name, station_distance)),1,reverse=False)
#Task1C

# a function that returns a list of stations (type <MonitoringStation)
#within radius r from a geographic coordinate x.
def stations_within_radius(stations, centre, r):
    stations_within_radius_list = []
    stations_by_distance_list = stations_by_distance(stations, centre)
    for i in range(len(stations_by_distance_list)):
        if stations_by_distance_list[i][1] <= r:
            stations_within_radius_list.append(stations_by_distance_list[i][0].name)
    return stations_within_radius_list

#Task1D
def rivers_with_station(stations):
    """Given a list of stations, this functions will return a set containing every river that has a monitoring station"""

    #initialise set of rivers
    set_rivers = set()

    for index in stations:
        set_rivers.add(index.river)

    return set_rivers

def stations_by_river(stations):
    """Given a list of stations, this function will return a dictionary of format River:[Stations]"""

    #Initialise dictionary
    dict_river_stations = {}

    for index in stations:
        if index.river in dict_river_stations:
            dict_river_stations[index.river].append(index)
        else:
            dict_river_stations[index.river] = [index]

    return dict_river_stations

    #Task1E
def rivers_by_station_number(stations, N):
    
    """Given a list of stations and the parameter N, this function will return a list comprised of the first N tuples (river name, number of stations)"""
    
    #Initialise empty dictionary
    dict_river_numberofstations = {}

    #Build dictionary of {River:number of stations}
    for index in stations:
        if index.river in dict_river_numberofstations:
            dict_river_numberofstations[index.river] += 1
        else:
            dict_river_numberofstations[index.river] = 1
        
    #Initialise empty list
    list_river_numberofstations = []

    #Convert dictionary to list
    for key, item in dict_river_numberofstations.items():
        list_river_numberofstations.append((key,item))

    #Define sort function
    def second_index(index):
        return index[1]

    #Sort list by station number descending
    list_river_numberofstations.sort(key=second_index, reverse=True)

    #Add first N-1 elements to output
    list_output = list_river_numberofstations[:(N-1)]

    #As long as the next element has the same number of stations as the final element, add it to the output
    i = N-1
    while i < len(list_river_numberofstations) and list_river_numberofstations[i][1] == list_river_numberofstations[N-1][1]: 
        list_output.append(list_river_numberofstations[i])
        i += 1
    
    return list_output

#Task 2G
def stations_by_town(stations):
    """Given a list of stations, this function will return a dictionary of format Towns:[Stations]"""

    #Initialise dictionary
    dict_town_stations = {}

    for index in stations:
        if type(index.town) == str:
            if index.town in dict_town_stations:
                dict_town_stations[index.town].append(index)
            else:
                dict_town_stations[index.town] = [index]

    return dict_town_stations
