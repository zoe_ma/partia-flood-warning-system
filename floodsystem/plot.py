import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.analysis import polyfit
import numpy as np
 

def plot_water_levels(station, dates, levels):

    # Plot
    plt.plot(dates, levels)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.hlines(station.typical_range[0], dates[0], dates[-1], label = 'typical low level')
    plt.hlines(station.typical_range[1], dates[0], dates[-1], label = 'typical high level')

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()
    plt.savefig("my-plot.pdf") 

def plot_water_level_with_fit(station, dates, levels, p):

    poly, d0 = polyfit(dates, levels, p)

    dates = matplotlib.dates.date2num(dates) - d0

    plt.plot(dates, levels, '.', color='c')

    fitted_dates = np.linspace(dates[0], dates[-1], len(dates))
    plt.plot(fitted_dates, poly(fitted_dates), color='r')

    typical_low = np.full(len(dates),station.typical_range[0])
    plt.plot(fitted_dates, typical_low, '--', color='g')
    typical_high = np.full(len(dates),station.typical_range[1])
    plt.plot(fitted_dates, typical_high, '--', color='g')

    plt.title(station.name)
    plt.xlabel("time")
    plt.ylabel("water level")

    # Display plot
    plt.show()