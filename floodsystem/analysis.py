import numpy as np
import matplotlib.dates

def polyfit(dates, levels, p):

    dates = matplotlib.dates.date2num(dates)
    d0 = dates[0]
    dates = dates-d0

    p_coeff = np.polyfit(dates, levels, p)

    poly = np.poly1d(p_coeff)

    return poly, d0